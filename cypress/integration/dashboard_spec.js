describe('Dashboard', () => {
  before(() => {
    cy.exec('node ./scripts/truncateDB.js')
    cy.exec('node ./scripts/createBooksWithUsersAndTeams.js')
  })
  it('shows dashboard', () => {
    cy.login('admin')
    cy.visit('/books')
    cy.contains('Books')
  })
  it('admin can add books', () => {
    cy.login('admin')
    cy.visit('/books')
    cy.get('[data-cy="add-book-btn"]').should('have.length', 1)
    cy.get('[data-cy="add-book-btn"]').should('not.be.disabled')
  })
  it('global production editor can add books', () => {
    cy.login('globalProductionEditor')
    cy.visit('/books')
    cy.get('[data-cy="add-book-btn"]').should('have.length', 1)
    cy.get('[data-cy="add-book-btn"]').should('not.be.disabled')
  })
  it('production editor can not add books', () => {
    cy.login('productionEditor')
    cy.visit('/books')
    cy.get('[data-cy="add-book-btn"]').should('have.length', 0)
  })
  it('copy editor can not add books', () => {
    cy.login('copyEditor')
    cy.visit('/books')
    cy.get('[data-cy="add-book-btn"]').should('have.length', 0)
  })
  it('author can not add books', () => {
    cy.login('author')
    cy.visit('/books')
    cy.get('[data-cy="add-book-btn"]').should('have.length', 0)
  })
  it('admin can see all three books', () => {
    cy.login('admin')
    cy.visit('/books')
    cy.get('[data-cy="book"]').should('have.length', 3)
  })
  it('global production editor can not see any book', () => {
    cy.login('globalProductionEditor')
    cy.visit('/books')
    cy.get('[data-cy="book"]').should('have.length', 0)
  })
  it('production editor can see only assigned books (1)', () => {
    cy.login('productionEditor')
    cy.visit('/books')
    cy.get('[data-cy="book"]').should('have.length', 1)
    cy.get('[data-cy="book"]').contains('Production Editor Book')
  })
  it('copy editor can see only assigned books (1)', () => {
    cy.login('copyEditor')
    cy.visit('/books')
    cy.get('[data-cy="book"]').should('have.length', 1)
    cy.get('[data-cy="book"]').contains('Copy Editor Book')
  })
  it('author can see only assigned books (1)', () => {
    cy.login('author')
    cy.visit('/books')
    cy.get('[data-cy="book"]').should('have.length', 1)
    cy.get('[data-cy="book"]').contains('Author Book')
  })
})

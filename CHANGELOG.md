# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [2.0.10](https://gitlab.coko.foundation/editoria/booksprints/compare/v2.0.9...v2.0.10) (2020-04-13)

### [2.0.9](https://gitlab.coko.foundation/editoria/booksprints/compare/v2.0.8...v2.0.9) (2020-03-24)

### [2.0.8](https://gitlab.coko.foundation/editoria/booksprints/compare/v2.0.7...v2.0.8) (2020-03-03)

<a name="2.0.7"></a>

## [2.0.7](https://gitlab.coko.foundation/editoria/booksprints/compare/v2.0.6...v2.0.7) (2019-11-06)

### Bug Fixes

- **app:** exporter and wax fixes ([70c5902](https://gitlab.coko.foundation/editoria/booksprints/commit/70c5902))

<a name="2.0.6"></a>

## [2.0.6](https://gitlab.coko.foundation/editoria/booksprints/compare/v2.0.5...v2.0.6) (2019-10-30)

### Bug Fixes

- **app:** new wax and comment fix ([4b5a44f](https://gitlab.coko.foundation/editoria/booksprints/commit/4b5a44f))

<a name="2.0.5"></a>

## [2.0.5](https://gitlab.coko.foundation/editoria/booksprints/compare/v2.0.4...v2.0.5) (2019-10-25)

### Bug Fixes

- **app:** dashboard refetching ([9d4f73f](https://gitlab.coko.foundation/editoria/booksprints/commit/9d4f73f))

<a name="2.0.4"></a>

## [2.0.4](https://gitlab.coko.foundation/editoria/booksprints/compare/v2.0.3...v2.0.4) (2019-10-25)

### Bug Fixes

- **app:** epubcheck payload and new wax ([4d5d404](https://gitlab.coko.foundation/editoria/booksprints/commit/4d5d404))

<a name="2.0.3"></a>

## [2.0.3](https://gitlab.coko.foundation/editoria/booksprints/compare/v2.0.2...v2.0.3) (2019-10-24)

### Bug Fixes

- **app:** mounted volumes fix ([fde5013](https://gitlab.coko.foundation/editoria/booksprints/commit/fde5013))

<a name="2.0.2"></a>

## [2.0.2](https://gitlab.coko.foundation/editoria/booksprints/compare/v2.0.1...v2.0.2) (2019-10-23)

### Bug Fixes

- **app:** jobs added, dynamic port for language tools ([35f0794](https://gitlab.coko.foundation/editoria/booksprints/commit/35f0794))

<a name="2.0.1"></a>

## [2.0.1](https://gitlab.coko.foundation/editoria/booksprints/compare/v2.1.0...v2.0.1) (2019-10-21)

### Bug Fixes

- **app:** booksprints config ([cb8a01c](https://gitlab.coko.foundation/editoria/booksprints/commit/cb8a01c))
- **app:** defaults for wax when custom component ([e703dac](https://gitlab.coko.foundation/editoria/booksprints/commit/e703dac))
